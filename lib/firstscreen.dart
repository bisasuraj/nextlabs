import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:nextLabs/auth.dart';
import 'package:nextLabs/loading.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:nextLabs/globals.dart' as globals;
import 'dart:convert' as convert;

class FirstScreen extends StatefulWidget {
  @override
  _FirstScreenState createState() => _FirstScreenState();
}

class _FirstScreenState extends State<FirstScreen> {
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    return loading
        ? Loading()
        : Scaffold(
            appBar: AppBar(
              automaticallyImplyLeading: false,
              title: Text('push data to google sheet'),
              actions: [
                Padding(
                    padding: EdgeInsets.only(right: 20.0),
                    child: GestureDetector(
                      onTap: () async {
                        Future.delayed(Duration.zero, () {
                          AuthService().signOutGoogle();
                        });
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.logout,
                        size: 26.0,
                      ),
                    ))
              ],
            ),
            body: Container(
              child: Center(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                  child: RaisedButton(
                      child: Text('send data'),
                      onPressed: () async {
                        setState(() {
                          loading = true;
                        });
                        //get data and send it to google sheet
                        //getting time...
                        final DateTime now = DateTime.now();
                        //print(now.hour+now.minute);
                        String mylocation;
                        String time =
                            now.hour.toString() + ':' + now.minute.toString();

                        //getting coordinates...
                        print('shit');
                        try {
                          Position position =
                              await Geolocator.getCurrentPosition(
                                  desiredAccuracy: LocationAccuracy.high);
                          mylocation = position.toString();
                          print(mylocation.toString());
                        } catch (e) {
                          print('Location not shared');
                        }
                        //throwing the details into the google sheet
                        String e = globals.email;
                        String p = mylocation.toString();

                        String dataToPush =
                            "?email=$e&time=$time&coordinates=$p";
                        String url =
                            "https://script.google.com/macros/s/AKfycbxyGPsJgJ7WTlqypsxejT2h5fJtiLPHZ2JZO0VYNKlhrHr0aXV5/exec";
                        try {
                          await http.get(url + dataToPush).then((response) {
                            //print(convert.jsonDecode(response.body)['status']);
                            print(response.body.toString());
                          });
                        } catch (e) {
                          print('error here?');
                          print(e);
                        }

                        setState(() {
                          loading = false;
                        });
                      }),
                ),
              ),
            ),
          );
  }
}
