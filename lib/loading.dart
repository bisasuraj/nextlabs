import 'package:flutter/material.dart';

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.grey[700],
        child: Center(
          child: Text(
            'Loading...',
            style: TextStyle(fontSize: 25),
          ),
        ),
      ),
    );
  }
}
